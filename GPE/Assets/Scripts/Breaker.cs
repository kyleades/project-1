﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaker : MonoBehaviour {
    // declare vars
    public GameObject ship;
    private Movement mover;
	// Use this for initialization
	void Start () {
        // hopefully grabbing the ability to move/transform
        mover = GetComponent<Movement>();
	}
	
	// Update is called once per frame
	void Update () {
        // break the game object with Q
		if(Input.GetKeyDown(KeyCode.Q))
        {
            ship.SetActive(false);
        }
        // turn off movement with P
        if (Input.GetKeyDown(KeyCode.P))
        {
            // disable movement somehow
            // re-enable when P is pressed again
            mover.enabled = !mover.enabled;
            

        }
    }
}
