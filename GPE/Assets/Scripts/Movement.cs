﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    // public speed var
    public float speed = .05f;
    // variable to hold transform
    private Transform tf;
    // var to hold spriterender
    private SpriteRenderer flipper;
	// Use this for initialization
	void Start () {
        // get the transform component
        tf = GetComponent<Transform>();
        flipper = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        // move to the right every draw at .05 speed. Slowly.
        if(Input.GetKey(KeyCode.RightArrow))
        {
            tf.position = tf.position + (Vector3.right * speed);
            flipper.flipX = false;
        }
        // move left
        // include a x axis flip transform if I can figure it out
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            tf.position = tf.position + (Vector3.left * speed);
            flipper.flipX = true;
        }
        // move up
        if(Input.GetKey(KeyCode.UpArrow))
        {
            tf.position = tf.position + (Vector3.up * speed);
        }
        // move down
        if(Input.GetKey(KeyCode.DownArrow))
        {
            tf.position = tf.position + (Vector3.down * speed);
        }
        // space resets to origin
        if(Input.GetKey(KeyCode.Space))
        {
            tf.position = Vector3.zero;
        }
        // shift key dodge one unit
        // hold shift and tap arrows
        // right
        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.RightArrow))
        {
            tf.position = tf.position + Vector3.right;
        }
        // left
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.LeftArrow))
        {
            tf.position = tf.position + Vector3.left;
        }
        // up
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.UpArrow))
        {
            tf.position = tf.position + Vector3.up;
        }
        // down
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.DownArrow))
        {
            tf.position = tf.position + Vector3.down;
        }
    }
}
