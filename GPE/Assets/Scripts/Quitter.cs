﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Quitter : MonoBehaviour {
	// Update is called once per frame
	void Update () {
        // Try to get a quit key to work
        // Using Get Button to try 
        if (Input.GetButtonDown("quit"))
        {
            Application.Quit();
        }
    }
}
